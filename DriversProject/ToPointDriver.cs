﻿using FIRADriverLibrary;
using Geometry;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace Driver
{
    internal class ToPointDriver
    {
        private const double WheelsHalfDistance = 0.034;
        private const double RobotCollisionAvoidanceDistance = 0.2;

        private const double MaxVelocity = 1.0;
        private const double TinyVelocity = 0.3; //below robot will change direction if needed, above will turn
        private const double FreeCurveFactor = 0.4;
/*
        private static double collisionDetectionTime = 0.5;
*/

        public static void DriveVelocity(FIRARobotState robot, double x, double y, double velocity)
        {
            double linearVel = (robot.leftSpeed + robot.rightSpeed)/2;
            double deltaVel = (robot.leftSpeed - robot.rightSpeed)/2;

            CalculateControlNoObstacles(robot, x, y, velocity, ref linearVel, ref deltaVel);

            robot.leftSpeed = linearVel + deltaVel;
            robot.rightSpeed = linearVel - deltaVel;
        }


        private static void CalculateControlNoObstacles(FIRARobotState robot, double x, double y, double velocity,
            ref double linearVel, ref double deltaVel)
        {
            Vector2D robotVersor = new Vector2D(robot.angle);
            Vector2D targetVector = new Vector2D(x - robot.x, y - robot.y);

            double angleRobotTarget = Vector2D.AngleBetween(robotVersor, targetVector);

            linearVel = Math.Sign(Math.Cos(angleRobotTarget))*Math.Min(MaxVelocity, velocity);
            deltaVel = FreeCurveFactor*Math.Sin(angleRobotTarget)*linearVel;

            if (targetVector.Length < RobotCollisionAvoidanceDistance) // sharpen turn if close to target
            {
                double sharpenFactor = (RobotCollisionAvoidanceDistance - targetVector.Length)/
                                       RobotCollisionAvoidanceDistance;
                deltaVel *= 1 + sharpenFactor;
                linearVel *= 0.5 + 0.5*(1 - sharpenFactor);
            }

            if (Math.Abs(robot.leftSpeed + robot.rightSpeed)/2 > TinyVelocity) // robot at motion
            {
                if (Math.Sign(robot.leftSpeed + robot.rightSpeed) != Math.Sign(linearVel)) // invert motion
                {
                    linearVel = -linearVel;
                    deltaVel = Math.Sign(Math.Sin(angleRobotTarget))*FreeCurveFactor*linearVel;
                }
            }
        }
    }
}
