﻿using System.Drawing;
using FIRADriverLibrary;
using Geometry;

namespace Driver
{
    /// <summary>
    /// ball only for now
    /// </summary>
    public class Predicator
    {
        private const double BallOnColisionVelocityAttenuation = 0.4;

        private readonly FIRADriver _firaDriver;
        private readonly RectangleF _pitchArea;

        public Predicator(FIRADriver firaDriver, RectangleF pitchArea)
        {
            _firaDriver = firaDriver;
            _pitchArea = pitchArea;
        }

        private Point2D _prevBallPosition;
        private double _prevTime;

        private double _vx, _vy;

        public void Refresh()
        {
            if (_prevTime == _firaDriver.time)
                return;

            _vx = _vx * 5 + (_firaDriver.ballX - _prevBallPosition.x) / (_firaDriver.time - _prevTime);
            _vx /= 6;
            _vy = _vy * 5 + (_firaDriver.ballY - _prevBallPosition.y) / (_firaDriver.time - _prevTime);
            _vy /= 6;

            _prevBallPosition.x = _firaDriver.ballX;
            _prevBallPosition.y = _firaDriver.ballY;
            _prevTime = _firaDriver.time;
        }

        public double VX
        {
            get { return _vx; }
        }
        public double VY
        {
            get { return _vy; }
        }

        public Vector2D V
        {
            get { return new Vector2D(_vx, _vy); }
        }

        public Point2D GetBallPosition(double eta)
        {
            var ballPosition = new Point2D(_firaDriver.ballX, _firaDriver.ballY);
            ballPosition.x += eta * _vx;
            ballPosition.y += eta * _vy;

            // walls collisions
            bool changed;
            do
            {
                changed = false;
                if (ballPosition.x < _pitchArea.Left)
                {
                    ballPosition.x = _pitchArea.Left + (_pitchArea.Left - ballPosition.x) * BallOnColisionVelocityAttenuation;
                    changed = true;
                }
                if (ballPosition.x > _pitchArea.Right)
                {
                    ballPosition.x = _pitchArea.Right - (ballPosition.x - _pitchArea.Right) * BallOnColisionVelocityAttenuation;
                    changed = true;
                }

                if (ballPosition.y < _pitchArea.Top)
                {
                    ballPosition.y = _pitchArea.Top + (_pitchArea.Top - ballPosition.y) * BallOnColisionVelocityAttenuation;
                    changed = true;
                }
                if (ballPosition.y > _pitchArea.Bottom)
                {
                    ballPosition.y = _pitchArea.Bottom - (ballPosition.y - _pitchArea.Bottom) * BallOnColisionVelocityAttenuation;
                    changed = true;
                }

            } while (changed);

            return ballPosition;
        }

    }
}

