using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.IO;
using System.Net;
using FIRADriverLibrary;

namespace Driver
{


    public class CreateFiraDriverForm : System.Windows.Forms.Form
    {

        private FIRADriver _firaDriver;
        public FIRADriver FiraDriver
        {
            get { return _firaDriver; }
        }

        private Label _label2;
        private TextBox _portTextBox;
        private TextBox _ipTextBox;
        private Label _label1;
        private Button _connectRoBossButton;


        public CreateFiraDriverForm()
        {
            InitializeComponent();
            try
            {
                IPAddress[] adresses = Dns.GetHostEntry(Dns.GetHostName()).AddressList;
                _ipTextBox.Text = adresses[adresses.Length - 1].ToString();
            }
            catch { }
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._connectRoBossButton = new System.Windows.Forms.Button();
            this._label2 = new System.Windows.Forms.Label();
            this._portTextBox = new System.Windows.Forms.TextBox();
            this._ipTextBox = new System.Windows.Forms.TextBox();
            this._label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // connectRoBOSSButton
            // 
            this._connectRoBossButton.Location = new System.Drawing.Point(281, 30);
            this._connectRoBossButton.Name = "_connectRoBossButton";
            this._connectRoBossButton.Size = new System.Drawing.Size(75, 23);
            this._connectRoBossButton.TabIndex = 3;
            this._connectRoBossButton.Text = "connect";
            this._connectRoBossButton.Click += new System.EventHandler(this.connectRoBOSSButton_Click);
            // 
            // label2
            // 
            this._label2.Location = new System.Drawing.Point(173, 29);
            this._label2.Name = "_label2";
            this._label2.Size = new System.Drawing.Size(31, 24);
            this._label2.TabIndex = 0;
            this._label2.Text = "port";
            this._label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // portTextBox
            // 
            this._portTextBox.Location = new System.Drawing.Point(210, 33);
            this._portTextBox.Name = "_portTextBox";
            this._portTextBox.Size = new System.Drawing.Size(48, 20);
            this._portTextBox.TabIndex = 2;
            this._portTextBox.Text = "4468";
            // 
            // ipTextBox
            // 
            this._ipTextBox.Location = new System.Drawing.Point(35, 33);
            this._ipTextBox.Name = "_ipTextBox";
            this._ipTextBox.Size = new System.Drawing.Size(136, 20);
            this._ipTextBox.TabIndex = 2;
            this._ipTextBox.Text = "controller ip address";
            // 
            // label1
            // 
            this._label1.Location = new System.Drawing.Point(12, 28);
            this._label1.Name = "_label1";
            this._label1.Size = new System.Drawing.Size(24, 24);
            this._label1.TabIndex = 0;
            this._label1.Text = "ip";
            this._label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // CreateFiraDriverForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(368, 68);
            this.Controls.Add(this._connectRoBossButton);
            this.Controls.Add(this._label1);
            this.Controls.Add(this._label2);
            this.Controls.Add(this._ipTextBox);
            this.Controls.Add(this._portTextBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CreateFiraDriverForm";
            this.Text = "Create FiraDriver ";
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion


        private void connectRoBOSSButton_Click(object sender, System.EventArgs e)
        {
            _firaDriver = new FIRADriver();
            if (0 > _firaDriver.ConnectToRoBOSS(_ipTextBox.Text, _portTextBox.Text))
            {
                MessageBox.Show("An error occured while connecting to RoBOSS:\n" + FIRADriver.lastError, "Failed to create FiraController!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DialogResult = DialogResult.OK;
        }

    }
}
