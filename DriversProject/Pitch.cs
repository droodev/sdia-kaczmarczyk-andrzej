﻿using System.Drawing;
using Geometry;

namespace Driver
{
    abstract class Pitch
    {
        private const float X = 0.15f;
        private const float Y = 0.0f;
        private const float Width = 2.2f;
        private const float Height = 1.8f;

        public static readonly float GoalWidth = 0.4f;
        public static readonly RectangleF Area = new RectangleF(X,Y,Width,Height);
        
        public static readonly Point2D UpperLeftPoint = new Point2D(X,Y);
        public static readonly Point2D UpperRightPoint = new Point2D(X+Width, Y);
        public static readonly Point2D BottomLeftPoint = new Point2D(X, Y+Height);
        public static readonly Point2D BottomRightPoint = new Point2D(X+Width, Y+Height);

        public static readonly Point2D LeftUpperGoalPoint = new Point2D(X, Y + (Height - GoalWidth) / 2);
        public static readonly Point2D LeftLowerGoalPoint = new Point2D(X, Y + (Height + GoalWidth) / 2);

        public static readonly Point2D RightUpperGoalPoint = new Point2D(X + Width, Y + (Height - GoalWidth) / 2);
        public static readonly Point2D RightLowerGoalPoint = new Point2D(X + Width, Y + (Height + GoalWidth) / 2);

        public static readonly Segment2D UpperSegment = new Segment2D(UpperLeftPoint, UpperRightPoint);
        public static readonly Segment2D BottomSegment = new Segment2D(BottomLeftPoint, BottomRightPoint);
        public static readonly Segment2D LeftUpperSegment = new Segment2D(UpperLeftPoint, LeftUpperGoalPoint);
        public static readonly Segment2D LeftLowerSegment = new Segment2D(LeftLowerGoalPoint, BottomLeftPoint);
        public static readonly Segment2D RightUpperSegment = new Segment2D(UpperRightPoint, RightUpperGoalPoint);
        public static readonly Segment2D RightLowerSegment = new Segment2D(RightLowerGoalPoint, BottomRightPoint);




    }
}
