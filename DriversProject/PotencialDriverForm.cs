using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Threading;
using FIRADriverLibrary;
using Geometry;


namespace Driver
{
	public class PotencialDriverForm : System.Windows.Forms.Form
	{
		private const float robotDiameter = 0.075f;
		private const float ballDiameter = 0.036f;
		private System.ComponentModel.IContainer components;

		private System.Windows.Forms.Panel legtPanel;
		private System.Windows.Forms.Panel viewPanel;
		private System.Windows.Forms.Splitter splitter1;
		private System.Windows.Forms.ListBox robotsListBox;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label statuSLabel;
		private System.Windows.Forms.Button requestButton;
		private System.Windows.Forms.Label rightStatusLabel;
		private System.Windows.Forms.Label middleStatusLabel;
		private System.Windows.Forms.Button releaseButton;
		private System.Windows.Forms.Timer interfaceRefreshTimer;

        private Point2D _goal = new Point2D(0.0, 0.9);
	
		private double _oldTime;
		private Thread refreshThread = null;
		private bool _killThread = false;
		private readonly Mutex _firaDriverMutex = null;

		private readonly FIRADriver _firaDriver;

		private Bitmap viewBitmap;
		private Graphics viewPanelGraphics;

		private float viewScale;
		private PointF viewOffset;

		private readonly Brush _pithBrush = (new Pen(Color.Black)).Brush;
		private readonly Pen _pithPen = new Pen(Color.LightGray, 3);
		private readonly Pen _robotPen = new Pen(Color.LightGreen, 1);
		private readonly Pen _theRobotPen = new Pen(Color.Blue, 2);
		private readonly Pen _ballPen = new Pen(Color.Orange, 1);
        private readonly Pen _predBallPen = new Pen(Color.Gold, 1);
		private readonly Pen _targetPen = new Pen(Color.Red, 1);
		private readonly Pen _wallPen = new Pen(Color.Salmon,3);
		private readonly Brush _additionalPointsBrush = (new Pen(Color.Salmon,0.0f)).Brush;

		private int robotDiameterView;
		private int ballDiameterView;


		int _movingRobotId = -1;
        private System.Windows.Forms.Button moveRobotButton;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.NumericUpDown fpsNumericUpDown;

        double targetTime = 0;
		private System.Windows.Forms.NumericUpDown velocityNumericUpDown;
        private Button button1;
		Point2D targetPoint;

	    private readonly ToLinePdDriver _lineDriver;

	    private readonly ToPointDriver _pointDriver;

	    private readonly ToPointObstaclesAvoidingDriver _obstaclesAvoidingDriver;

	    private readonly Predicator _predicator;
	    private const int PredictionEta = 1;

        private readonly List<FIRARobotState> _robotsInTeam = new List<FIRARobotState>();


	    public PotencialDriverForm(FIRADriver firaDriver)
		{
			_firaDriver = firaDriver;

            _lineDriver = new ToLinePdDriver(firaDriver, Pitch.Area.Width);
            _pointDriver = new ToPointDriver();
            _obstaclesAvoidingDriver = new ToPointObstaclesAvoidingDriver(firaDriver);
            _predicator = new Predicator(firaDriver, Pitch.Area);

			InitializeComponent();

			FillRobotList();
			CreateViewDrawer();

			_firaDriverMutex = new Mutex();

			refreshThread = new Thread(RefreshThread) {Priority = ThreadPriority.AboveNormal};
	        refreshThread.Start();

			interfaceRefreshTimer.Enabled = true;
		}

		private void RefreshThread()
		{
			_oldTime = -1.0;
			while (true)
			{
			    _firaDriverMutex.WaitOne();
				_firaDriver.Step();
				if (_firaDriver.time > _oldTime)
				{
   
					if (_movingRobotId >= 0)
					{

					    var predicatedBallPoint = _predicator.GetBallPosition(PredictionEta/2);

					    const double linearVelocity = 2.0;
					    
                        

                        
                        //var ballPoint = new Point2D(_firaDriver.ballX, _firaDriver.ballY);
                        //var currentRobotPoint = new Point2D(_firaDriver.firaRobots[_movingRobotId].x, _firaDriver.firaRobots[_movingRobotId].y);

					    bool shotDecision = false;
					    FIRARobotState actionRobot = null;
					    double maxFitness = 0.0;
                        const double fitnessBorder = 0.7;
					    foreach (var currentRobot in _robotsInTeam)
					    {
                            double shotFitness = _lineDriver.CalculateFitness(currentRobot, _goal.x, _goal.y, linearVelocity, predicatedBallPoint);
                            double prepareFitness = _obstaclesAvoidingDriver.CalculateFitness(currentRobot, _goal.x, _goal.y, linearVelocity, predicatedBallPoint);
                            if(shotDecision)
                            {
                                maxFitness = Math.Max(shotFitness, maxFitness);
                                if (maxFitness == shotFitness)
                                {
                                    actionRobot = currentRobot;
                                }
                                continue;
                            }

					        if (shotFitness > fitnessBorder)
					        {
					            Console.WriteLine("SHOT fitness: " + shotFitness);
					            shotDecision = true;
					            actionRobot = currentRobot;
					            maxFitness = shotFitness;
					        }
					        else
					        {
					            maxFitness = Math.Max(prepareFitness, maxFitness);
                                if (maxFitness == prepareFitness)
                                {
                                    actionRobot = currentRobot;
                                }
					        }
                            
					    }
					    if (shotDecision)
					    {
					        _lineDriver.DriveToLine(actionRobot, _goal.x, _goal.y, linearVelocity,
					            _predicator.GetBallPosition(PredictionEta/2));
					    }
					    else
					    {

					        var ballToGoalVector = new Vector2D(predicatedBallPoint, _goal);
					        ballToGoalVector.Normalize();
					        ballToGoalVector.Inverse();
					        const double scaleFactor = 0.7;
					        var displacementVector = new Vector2D(ballToGoalVector.x*scaleFactor, ballToGoalVector.y*scaleFactor);
					        var quiteGoodPoint = predicatedBallPoint.GetTranslatedPoint(displacementVector);
					        Console.WriteLine("Prepare better state to: " + quiteGoodPoint);
					        _obstaclesAvoidingDriver.DriveVelocity(actionRobot, quiteGoodPoint.x,
					            quiteGoodPoint.y, linearVelocity);
					    }
                        foreach (var currentRobot in _robotsInTeam)
                        {
                            if (currentRobot.Equals(actionRobot))
                            {
                                continue;
                            }
                            currentRobot.rightSpeed = 0;
                            currentRobot.leftSpeed = 0;

                        }
					    //Console.WriteLine("jump");
                        
                        //change

                        //targetPoint = Predicator.GetBallPosition(1);
                        //double diff = targetPoint.GetDistance(_firaDriver.ballX, _firaDriver.ballY);

						//if (targetPoint.GetDistance(_firaDriver.firaRobots[_movingRobotId].x, _firaDriver.firaRobots[_movingRobotId].y) < 0.05)
                        //if (Predicator.GetBallPosition(0.2).GetDistance(_firaDriver.firaRobots[movingRobotId].x, _firaDriver.firaRobots[movingRobotId].y) < diff / 2)
					    /*
                        if(ballPoint.GetDistance(currentRobotPoint)<0.08)
						{
							_firaDriver.firaRobots[_movingRobotId].leftSpeed = 0;
							_firaDriver.firaRobots[_movingRobotId].rightSpeed = 0;
							_movingRobotId =-1;
						}
						else
						{
							//ToPointDriver.DriveVelocity(_firaDriver.firaRobots[movingRobotId], targetPoint.x, targetPoint.y, (double)0.5);
                            //ToPointObstaclesAvoidingDriver.DriveVelocity(_firaDriver.firaRobots[movingRobotId], targetPoint.x, targetPoint.y, (double)0.5);
						}
                        */
//						if (targetTime > _firaDriver.time)
//						{
//							ToPointObstaclesAvoidingDriver.Drive(movingRobotId, targetPoint.x, targetPoint.y, targetTime-_firaDriver.time);
//						}
//						else
//						{
//							_firaDriver.firaRobots[movingRobotId].leftSpeed = 0;
//							_firaDriver.firaRobots[movingRobotId].rightSpeed = 0;
//						}
					}
                    _predicator.Refresh();
                    try
                    {
                        rightStatusLabel.Text = _firaDriver.time.ToString("f3");    
                    }
                    catch (Exception e)
                    {
                        Console.Write(e.StackTrace);
                    }
                    _oldTime = _firaDriver.time + 1 / (double)fpsNumericUpDown.Value;
				    _firaDriverMutex.ReleaseMutex();
				}
				else 
				{
					_firaDriverMutex.ReleaseMutex();	//before
					Thread.Sleep(1);
				}
				if (_killThread)
					return;
			}
		}


		private void FillRobotList()
		{
			foreach (FIRARobotState robot in _firaDriver.firaRobots)
			{
				robotsListBox.Items.Add(robot);
			}
		}

		private void CreateViewDrawer()
		{
			if (viewBitmap != null)
				viewBitmap.Dispose();
			if (viewPanelGraphics != null)
				viewPanelGraphics.Dispose();

			viewPanelGraphics = viewPanel.CreateGraphics();
			viewPanelGraphics.Clear(SystemColors.ControlDarkDark);
			viewBitmap = new Bitmap(Math.Max(1,viewPanel.Width), Math.Max(1,viewPanel.Height));
			Graphics viewBitmapGraphics = Graphics.FromImage(viewBitmap);

			viewScale = Math.Min(viewPanel.Width / (1.2f * Pitch.Area.Width),
			    viewPanel.Height / (1.1f * Pitch.Area.Height));
            viewOffset = new PointF(-Pitch.Area.X + Pitch.Area.Width * 0.1f,
                -Pitch.Area.Y + Pitch.Area.Width * 0.05f);


			//goals border
            viewBitmapGraphics.DrawRectangle(_pithPen, Real2ViewX(Pitch.Area.X - Pitch.Area.Width * 0.05f), Real2ViewY(Pitch.Area.Y + Pitch.Area.Height / 2 - Pitch.GoalWidth / 2),
                (Pitch.Area.Width * 1.1f) * viewScale, Pitch.GoalWidth * viewScale);
			//pitch
            viewBitmapGraphics.FillRectangle(_pithBrush, Real2ViewX(Pitch.Area.X), Real2ViewY(Pitch.Area.Y),
                Pitch.Area.Width * viewScale, Pitch.Area.Height * viewScale);
            viewBitmapGraphics.DrawRectangle(_pithPen, Real2ViewX(Pitch.Area.X), Real2ViewY(Pitch.Area.Y),
                Pitch.Area.Width * viewScale, Pitch.Area.Height * viewScale);

			//goals
            viewBitmapGraphics.FillRectangle(_pithBrush, Real2ViewX(Pitch.Area.X - Pitch.Area.Width * 0.05f), Real2ViewY(Pitch.Area.Y + Pitch.Area.Height / 2 - Pitch.GoalWidth / 2),
                (Pitch.Area.Width * 1.1f) * viewScale, Pitch.GoalWidth * viewScale);
			
			robotDiameterView = (int)(robotDiameter * viewScale);
			ballDiameterView = (int)(ballDiameter * viewScale);


			viewBitmapGraphics.Dispose();
			viewPanel_Paint(this,null);
		}

		private int Real2ViewX(double realX)
		{
			return (int)((realX + viewOffset.X) * viewScale);
		}
		private int Real2ViewY(double realY)
		{
			return (int)((realY + viewOffset.Y) * viewScale);
		}


		private double View2RealX(int x)
		{
			return (double)x / viewScale - viewOffset.X;
		}
		private double View2RealY(int y)
		{
			return (double)y / viewScale - viewOffset.Y;
		}




		protected override void Dispose( bool disposing )
		{
			interfaceRefreshTimer.Enabled = false;
			_killThread = true;

			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.legtPanel = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.fpsNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.velocityNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.moveRobotButton = new System.Windows.Forms.Button();
            this.requestButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.robotsListBox = new System.Windows.Forms.ListBox();
            this.releaseButton = new System.Windows.Forms.Button();
            this.viewPanel = new System.Windows.Forms.Panel();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.statuSLabel = new System.Windows.Forms.Label();
            this.rightStatusLabel = new System.Windows.Forms.Label();
            this.middleStatusLabel = new System.Windows.Forms.Label();
            this.interfaceRefreshTimer = new System.Windows.Forms.Timer(this.components);
            this.legtPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fpsNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.velocityNumericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // legtPanel
            // 
            this.legtPanel.Controls.Add(this.button1);
            this.legtPanel.Controls.Add(this.fpsNumericUpDown);
            this.legtPanel.Controls.Add(this.label3);
            this.legtPanel.Controls.Add(this.velocityNumericUpDown);
            this.legtPanel.Controls.Add(this.label2);
            this.legtPanel.Controls.Add(this.moveRobotButton);
            this.legtPanel.Controls.Add(this.requestButton);
            this.legtPanel.Controls.Add(this.label1);
            this.legtPanel.Controls.Add(this.robotsListBox);
            this.legtPanel.Controls.Add(this.releaseButton);
            this.legtPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.legtPanel.Location = new System.Drawing.Point(0, 0);
            this.legtPanel.Name = "legtPanel";
            this.legtPanel.Size = new System.Drawing.Size(184, 457);
            this.legtPanel.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(8, 279);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 10;
            this.button1.Text = "Go";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // fpsNumericUpDown
            // 
            this.fpsNumericUpDown.Location = new System.Drawing.Point(112, 8);
            this.fpsNumericUpDown.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.fpsNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.fpsNumericUpDown.Name = "fpsNumericUpDown";
            this.fpsNumericUpDown.Size = new System.Drawing.Size(64, 20);
            this.fpsNumericUpDown.TabIndex = 8;
            this.fpsNumericUpDown.Value = new decimal(new int[] {
            25,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(8, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 23);
            this.label3.TabIndex = 7;
            this.label3.Text = "communicator FPS:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // velocityNumericUpDown
            // 
            this.velocityNumericUpDown.DecimalPlaces = 1;
            this.velocityNumericUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.velocityNumericUpDown.Location = new System.Drawing.Point(56, 320);
            this.velocityNumericUpDown.Maximum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.velocityNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.velocityNumericUpDown.Name = "velocityNumericUpDown";
            this.velocityNumericUpDown.Size = new System.Drawing.Size(48, 20);
            this.velocityNumericUpDown.TabIndex = 6;
            this.velocityNumericUpDown.Value = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(8, 320);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 23);
            this.label2.TabIndex = 5;
            this.label2.Text = "vel:";
            // 
            // moveRobotButton
            // 
            this.moveRobotButton.Location = new System.Drawing.Point(104, 320);
            this.moveRobotButton.Name = "moveRobotButton";
            this.moveRobotButton.Size = new System.Drawing.Size(72, 22);
            this.moveRobotButton.TabIndex = 3;
            this.moveRobotButton.Text = "move robot";
            this.moveRobotButton.Click += new System.EventHandler(this.moveRobotButton_Click);
            // 
            // requestButton
            // 
            this.requestButton.Location = new System.Drawing.Point(8, 240);
            this.requestButton.Name = "requestButton";
            this.requestButton.Size = new System.Drawing.Size(56, 23);
            this.requestButton.TabIndex = 2;
            this.requestButton.Text = "request";
            this.requestButton.Click += new System.EventHandler(this.requestButton_Click);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(0, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 16);
            this.label1.TabIndex = 1;
            this.label1.Text = "robots:";
            // 
            // robotsListBox
            // 
            this.robotsListBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.robotsListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.robotsListBox.ItemHeight = 16;
            this.robotsListBox.Location = new System.Drawing.Point(0, 56);
            this.robotsListBox.Name = "robotsListBox";
            this.robotsListBox.Size = new System.Drawing.Size(184, 180);
            this.robotsListBox.TabIndex = 0;
            // 
            // releaseButton
            // 
            this.releaseButton.Location = new System.Drawing.Point(72, 240);
            this.releaseButton.Name = "releaseButton";
            this.releaseButton.Size = new System.Drawing.Size(56, 23);
            this.releaseButton.TabIndex = 2;
            this.releaseButton.Text = "release";
            this.releaseButton.Click += new System.EventHandler(this.releaseButton_Click);
            // 
            // viewPanel
            // 
            this.viewPanel.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.viewPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.viewPanel.Location = new System.Drawing.Point(187, 0);
            this.viewPanel.Name = "viewPanel";
            this.viewPanel.Size = new System.Drawing.Size(581, 457);
            this.viewPanel.TabIndex = 1;
            this.viewPanel.TabStop = true;
            this.viewPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.viewPanel_Paint);
            this.viewPanel.MouseUp += new System.Windows.Forms.MouseEventHandler(this.viewPanel_MouseUp);
            this.viewPanel.Resize += new System.EventHandler(this.viewPanel_Resize);
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(184, 0);
            this.splitter1.MinExtra = 184;
            this.splitter1.MinSize = 184;
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 457);
            this.splitter1.TabIndex = 2;
            this.splitter1.TabStop = false;
            // 
            // statuSLabel
            // 
            this.statuSLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.statuSLabel.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.statuSLabel.Location = new System.Drawing.Point(0, 457);
            this.statuSLabel.Name = "statuSLabel";
            this.statuSLabel.Size = new System.Drawing.Size(328, 12);
            this.statuSLabel.TabIndex = 3;
            // 
            // rightStatusLabel
            // 
            this.rightStatusLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.rightStatusLabel.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.rightStatusLabel.Location = new System.Drawing.Point(656, 457);
            this.rightStatusLabel.Name = "rightStatusLabel";
            this.rightStatusLabel.Size = new System.Drawing.Size(112, 12);
            this.rightStatusLabel.TabIndex = 4;
            this.rightStatusLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.rightStatusLabel.DoubleClick += new System.EventHandler(this.rightStatusLabel_DoubleClick);
            // 
            // middleStatusLabel
            // 
            this.middleStatusLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.middleStatusLabel.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.middleStatusLabel.Location = new System.Drawing.Point(328, 457);
            this.middleStatusLabel.Name = "middleStatusLabel";
            this.middleStatusLabel.Size = new System.Drawing.Size(200, 12);
            this.middleStatusLabel.TabIndex = 5;
            // 
            // interfaceRefreshTimer
            // 
            this.interfaceRefreshTimer.Interval = 200;
            this.interfaceRefreshTimer.Tick += new System.EventHandler(this.interfaceRefreshTimer_Tick);
            // 
            // PotencialDriverForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(768, 469);
            this.Controls.Add(this.middleStatusLabel);
            this.Controls.Add(this.statuSLabel);
            this.Controls.Add(this.rightStatusLabel);
            this.Controls.Add(this.viewPanel);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.legtPanel);
            this.MinimumSize = new System.Drawing.Size(592, 352);
            this.Name = "PotencialDriverForm";
            this.Padding = new System.Windows.Forms.Padding(0, 0, 0, 12);
            this.Text = "Potencial Driver";
            this.Load += new System.EventHandler(this.PotencialDriverForm_Load);
            this.legtPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fpsNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.velocityNumericUpDown)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		[STAThread]
		static void Main() 
		{
			var createFiraDriverForm = new CreateFiraDriverForm();
			createFiraDriverForm.ShowDialog();
			FIRADriver firaDriver = createFiraDriverForm.FiraDriver;
			if (firaDriver != null)
			{
				Application.Run(new PotencialDriverForm(firaDriver));
				firaDriver.Dispose();
			}
		}

		private void requestButton_Click(object sender, System.EventArgs e)
		{
			var robot = robotsListBox.SelectedItem as FIRARobotState;
			if (robot == null)
				return;
			if (robot.owned)
			{
				MessageBox.Show("Robot already granted.");
				return;
			}
			Cursor = Cursors.WaitCursor;
			_firaDriverMutex.WaitOne();
			if (_firaDriver.RequestRobot(robot.portId) < 0)
			{
				MessageBox.Show("Error requesting robot: "+FIRADriver.lastError);
			}
		    _robotsInTeam.Add(robot);
			_firaDriverMutex.ReleaseMutex();
			Cursor = Cursors.Default;

			statuSLabel.Text = "Robot "+robot.portId+" granted!.";
		}

		private void releaseButton_Click(object sender, System.EventArgs e)
		{
			var robot = robotsListBox.SelectedItem as FIRARobotState;
			if (robot == null)
				return;
			if (!robot.owned)
			{
				MessageBox.Show("Robot not owned.");
				return;
			}
			_firaDriverMutex.WaitOne();
			_firaDriver.ReleaseRobot(robot.portId);
            _robotsInTeam.Remove(robot);
			_firaDriverMutex.ReleaseMutex();
			statuSLabel.Text = "Robot "+robot.portId+" released!.";		
		}

		private void viewPanel_Resize(object sender, System.EventArgs e)
		{
			CreateViewDrawer();
		}

		private void viewPanel_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			if (viewPanelGraphics==null && viewBitmap==null)
				return;
            
			viewPanelGraphics.DrawImageUnscaled(viewBitmap,0,0);

			foreach (FIRARobotState robot in _firaDriver.firaRobots)
			{
				viewPanelGraphics.DrawEllipse((robot.owned?_theRobotPen:_robotPen), Real2ViewX(robot.x)-robotDiameterView/2, Real2ViewY(robot.y)-robotDiameterView/2, robotDiameterView, robotDiameterView);
				viewPanelGraphics.DrawLine((robot.owned?_theRobotPen:_robotPen),  Real2ViewX(robot.x), Real2ViewY(robot.y), Real2ViewX(robot.x) + (float)(robotDiameterView*Math.Cos(robot.angle)), Real2ViewY(robot.y) + (float)(robotDiameterView*Math.Sin(robot.angle)));
			}
			
			viewPanelGraphics.DrawEllipse(_ballPen, Real2ViewX(_firaDriver.ballX)-ballDiameterView/2, Real2ViewY(_firaDriver.ballY)-ballDiameterView/2, ballDiameterView, ballDiameterView);
			viewPanelGraphics.DrawEllipse(_targetPen, Real2ViewX(targetPoint.x),Real2ViewY(targetPoint.y),1,1);

            //ADDED
            var predicatePoint = _predicator.GetBallPosition(PredictionEta);
            viewPanelGraphics.DrawEllipse(_predBallPen, Real2ViewX(predicatePoint.x) - ballDiameterView / 2, Real2ViewY(predicatePoint.y) - ballDiameterView / 2, ballDiameterView, ballDiameterView);

            viewPanelGraphics.DrawLine(_targetPen, new Point2D(Real2ViewX(_goal.x),Real2ViewY(_goal.y)), new Point2D(Real2ViewX(_firaDriver.ballX), Real2ViewY(_firaDriver.ballY)));

			lock(ToPointObstaclesAvoidingDriver.walls)
			{
				foreach (Wall2D wall in ToPointObstaclesAvoidingDriver.walls)
					viewPanelGraphics.DrawLine(_wallPen, Real2ViewX(wall.firstPoint.x), Real2ViewY(wall.firstPoint.y), Real2ViewX(wall.secondPoint.x), Real2ViewY(wall.secondPoint.y));
			}

			lock (ToPointObstaclesAvoidingDriver.additionalObstaclePoints)
			{
				foreach (Point2D point in ToPointObstaclesAvoidingDriver.additionalObstaclePoints)
					viewPanelGraphics.FillRectangle(_additionalPointsBrush, Real2ViewX(point.x - 0.04), Real2ViewY(point.y - 0.04), 0.08f*viewScale, 0.08f*viewScale);
			}							


			// draw locks
			FIRARobotState selectedRobot = null;
			try
			{
				selectedRobot = robotsListBox.Items[robotsListBox.SelectedIndex] as FIRARobotState;
			}
			catch
			{
				return;
			}
			if (selectedRobot == null || ToPointObstaclesAvoidingDriver.angleLocks==null)
				return;

			_firaDriverMutex.WaitOne();
			foreach (ToPointObstaclesAvoidingDriver.AngleLock angleLock in ToPointObstaclesAvoidingDriver.angleLocks)
			{
				viewPanelGraphics.DrawLine(_ballPen,  Real2ViewX(selectedRobot.x), Real2ViewY(selectedRobot.y), 
					Real2ViewX(selectedRobot.x) + (float)(Math.Cos(angleLock.lockVector.Angle + angleLock.halfAngle)*angleLock.lockVector.Length*viewScale),
					Real2ViewY(selectedRobot.y) + (float)(Math.Sin(angleLock.lockVector.Angle + angleLock.halfAngle)*angleLock.lockVector.Length*viewScale));
				viewPanelGraphics.DrawLine(_ballPen,  Real2ViewX(selectedRobot.x), Real2ViewY(selectedRobot.y), 
					Real2ViewX(selectedRobot.x) + (float)(Math.Cos(angleLock.lockVector.Angle - angleLock.halfAngle)*angleLock.lockVector.Length*viewScale),
					Real2ViewY(selectedRobot.y) + (float)(Math.Sin(angleLock.lockVector.Angle - angleLock.halfAngle)*angleLock.lockVector.Length*viewScale));
				
				viewPanelGraphics.DrawEllipse(_targetPen, Real2ViewX(ToPointObstaclesAvoidingDriver.checkPoint.x),Real2ViewY(ToPointObstaclesAvoidingDriver.checkPoint.y),2,2);


			}
			_firaDriverMutex.ReleaseMutex();



		}

		private void interfaceRefreshTimer_Tick(object sender, System.EventArgs e)
		{
			viewPanel_Paint(this,null);
		}


		private void viewPanel_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
		{
            _goal = new Point2D(View2RealX(e.X), View2RealY(e.Y));

		}

		private void moveRobotButton_Click(object sender, System.EventArgs e)
		{
			//targetTime = _firaDriver.time + (double)timeNumericUpDown.Value;
			_movingRobotId = robotsListBox.SelectedIndex;	
		}

		private void rightStatusLabel_DoubleClick(object sender, System.EventArgs e)
		{
			_oldTime = -1;
		}


		private void pitchBoundsButton_Click(object sender, System.EventArgs e)
		{
				lock(ToPointObstaclesAvoidingDriver.walls)
				{
					ToPointObstaclesAvoidingDriver.walls.Add(new Wall2D(
						new Point2D(Pitch.Area.Left, Pitch.Area.Top),
						new Point2D(Pitch.Area.Right, Pitch.Area.Top)));
					ToPointObstaclesAvoidingDriver.walls.Add(new Wall2D(
						new Point2D(Pitch.Area.Right, Pitch.Area.Top),
						new Point2D(Pitch.Area.Right, Pitch.Area.Bottom)));
					ToPointObstaclesAvoidingDriver.walls.Add(new Wall2D(
						new Point2D(Pitch.Area.Right, Pitch.Area.Bottom),
						new Point2D(Pitch.Area.Left, Pitch.Area.Bottom)));
					ToPointObstaclesAvoidingDriver.walls.Add(new Wall2D(
						new Point2D(Pitch.Area.Left, Pitch.Area.Bottom),
						new Point2D(Pitch.Area.Left, Pitch.Area.Top)));
				}
		
		}

		private void PotencialDriverForm_Load(object sender, System.EventArgs e)
		{
		
		}

        private void button1_Click(object sender, EventArgs e)
        {
            _movingRobotId = robotsListBox.SelectedIndex;
        }

	}
}
