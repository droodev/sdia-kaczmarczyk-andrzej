﻿using System.Drawing;
using FIRADriverLibrary;
using Geometry;
using System;

namespace Driver
{
    internal class ToLinePdDriver
    {
        private readonly FIRADriver _driver;
        private readonly float _maxDistance;

        private Point2D _ballPoint;
        //private Point2D _predictedBallPoint;
        private Point2D _goalPoint;
        private Point2D _robotPoint;
        private Line2D _shotLine;
        private Vector2D _robotVelocityVersor;
        private Vector2D _ballToGoalVector;
        private Vector2D _ballToGoalVersor;
        private Vector2D _goalToRobotVector;
        private Vector2D _robotToBallVersor;
        private Vector2D _robotToBallVector;
        private FIRARobotState _robot;

        public ToLinePdDriver(FIRADriver driver, float maxDistance)
        {
            _driver = driver;
            _maxDistance = maxDistance;
        }

        /// <summary>
        ///  Updates state of simple vectors and versors needed in computations in method
        /// </summary>
        private void UpdateState(FIRARobotState robot, double goalX, double goalY, Point2D predictedBallPoint)
        {
            //_predictedBallPoint = predictedBallPoint;
            //_ballPoint = new Point2D(_driver.ballX, _driver.ballY);
            _ballPoint = predictedBallPoint;
            _goalPoint = new Point2D(goalX, goalY);
            _robotPoint = new Point2D(robot.x, robot.y);

            _shotLine = new Line2D(_ballPoint, _goalPoint);
            _robotVelocityVersor = new Vector2D(robot.angle);

            _ballToGoalVector = new Vector2D(_ballPoint, _goalPoint);
            _ballToGoalVersor = _ballToGoalVector;
            _ballToGoalVersor.Normalize();

            _goalToRobotVector = new Vector2D(_goalPoint, _robotPoint);

            _robotToBallVector = new Vector2D(_robotPoint, _ballPoint);
            _robotToBallVersor = _robotToBallVector;
            _robotToBallVersor.Normalize();

            _robot = robot;
        }

        public void DriveToLine(FIRARobotState robot, double goalX, double goalY, double linearVelocity, Point2D predictedBallPoint)
        {
            UpdateState(robot, goalX, goalY, predictedBallPoint);

            var robotDistanceFromShotLine = _shotLine.GetPointDistance(_robot.x, _robot.y);

            var perpendicularToShotDirection = new Vector2D(_ballToGoalVersor.x, _ballToGoalVersor.y);
            perpendicularToShotDirection.MakePerpendicular();


            var orientationFromShotLine = _goalToRobotVector.Dot(perpendicularToShotDirection);
            int orientationFromShotLineModifier = Math.Sign(orientationFromShotLine);

            int movingDirectionModifier = Math.Sign(_ballToGoalVersor.Dot(_robotVelocityVersor));
            var movingVelocityVersor = _robotVelocityVersor*movingDirectionModifier;
            var anglePart = _ballToGoalVersor.AngleBetween(movingVelocityVersor);

            var normalizedRobotDistanceFromShotLine = robotDistanceFromShotLine/_maxDistance;
            normalizedRobotDistanceFromShotLine = - (normalizedRobotDistanceFromShotLine - 1)*
                                                  (normalizedRobotDistanceFromShotLine - 1)*
                                                  (normalizedRobotDistanceFromShotLine - 1)*
                                                  (normalizedRobotDistanceFromShotLine - 1) + 1;
            //var distanceFunctionValue = 2 * Math.Sqrt(normalizedRobotDistanceFromShotLine) / (1 + normalizedRobotDistanceFromShotLine);
            //var distanceFunctionValue = normalizedRobotDistanceFromShotLine;
            //var distanceFunctionValue = normalizedRobotDistanceFromShotLine*normalizedRobotDistanceFromShotLine*
            //                            (2 - normalizedRobotDistanceFromShotLine)*
            //                            (2 - normalizedRobotDistanceFromShotLine);
            var distanceFunctionValue =
                Math.Exp(-1.0/25/normalizedRobotDistanceFromShotLine/normalizedRobotDistanceFromShotLine);
            var distancePart = Math.PI/2*distanceFunctionValue*-orientationFromShotLineModifier;

            var steeringAngle = anglePart + distancePart;
            //Console.WriteLine("NP: " + normalizedRobotDistanceFromShotLine);
            //Console.WriteLine("DP: " + distancePart);

            //var inversedNormalizedDistance = Math.Max(0.2,1 - normalizedRobotDistanceFromShotLine*normalizedRobotDistanceFromShotLine);
            var inversedNormalizedDistance = 1 - normalizedRobotDistanceFromShotLine;

            //var angleModif = Math.Max(0, ballToRobotVersor.Dot(shotDirection));

            //Console.WriteLine(angleModif);
            var nearLineAnle = Math.Acos(_robotVelocityVersor.Dot(_ballToGoalVersor));
            //Console.WriteLine("A:" + nearLineAnle);

            var wiseModfi = (Math.Cos(nearLineAnle*4) + 1)/2;
            //Console.WriteLine(wiseModfi);
            //Console.WriteLine("ND: " + inversedNormalizedDistance);
            //Console.WriteLine(Math.Cos((robot.angle-Math.PI/2)/4)+1/2);
            linearVelocity = linearVelocity*
                             Math.Max(0.2, wiseModfi*inversedNormalizedDistance*inversedNormalizedDistance);
            //Console.WriteLine("V: " + linearVelocity);

            var linVelMod = nearLineAnle +
                            2*Math.Sqrt(normalizedRobotDistanceFromShotLine)/(1 + normalizedRobotDistanceFromShotLine);


            var steeringVelocityRatio = linearVelocity*(steeringAngle/Math.PI*Math.Min(1.0, linVelMod));

            //steering between 0 and 0.5


            //var steeringVelocityRatio = steeringPercent*steeringAngle;
            //turns right if goes forward
            robot.leftSpeed = movingDirectionModifier*(linearVelocity) - steeringVelocityRatio;
            robot.rightSpeed = movingDirectionModifier*(linearVelocity) + steeringVelocityRatio;
            //Console.WriteLine("Dp(Nd): " + distancePart + " Nd: " + normalizedDistance + " SA: " + steeringAngle + " PA: " + presetAngle + " vel: " + velocity.Angle + " sd: " + shotDirection.Angle + " DP: " + dp);
            //Console.WriteLine("SA: " + steeringAngle + " vel: " + robotVelocityDirection.Angle + " dof: " + movingForwardModifier+ " pao: " + anglePart+ " LS: " + robot.leftSpeed + " RS: " + robot.rightSpeed);
        }

        public double CalculateFitness(FIRARobotState robot, double goalX, double goalY, double velocity, Point2D predictedBallPosition)
        {
            UpdateState(robot, goalX, goalY, predictedBallPosition);
            

            var ballTarget = BallTargetReachabilityPart(0.4);
            var targetInTime = TargetInTimeReachingPart(velocity, 1);
            var robotBall = RobotBallReachabilityPart();
            var robotBallDistance = RobotToBallDistanceReachabilityPart();


            const double targetInTimeCoeff = 0.3;
            const double ballTargetCoeff = 0.3;
            const double robotBallCoeff = 0.1;
            const double robotBallDistanceCoeff = 0.3;

            Console.WriteLine(String.Format("BT: {0}, TiT: {1}, RB: {2}, RBD: {3}", ballTarget, targetInTime, robotBall, robotBallDistance));

            var sum = ballTarget*ballTargetCoeff + targetInTime*targetInTimeCoeff + robotBall*robotBallCoeff + robotBallDistance*robotBallDistanceCoeff;
            var product = ballTarget * targetInTime * robotBall * robotBallDistance;
            if (Math.Abs(product - 0.0) <= Double.Epsilon)
            {
                return 0.0;
            }
            else
            {
                return sum;
            }
        }

        private double RobotToBallDistanceReachabilityPart()
        {
            var robotToBallDistance = _robotToBallVector.Length;
            var normalizedDistance = 1 - robotToBallDistance / _maxDistance;
            return Math.Max(0, _robotToBallVersor.Dot(_ballToGoalVersor)) * normalizedDistance;
        }

        private double BallTargetReachabilityPart(double checkAreaRadius)
        {
            //it is maximum dot product of checkAreaRadius vector and ball to goal vector
            double normalizationCoefficient = checkAreaRadius*_ballToGoalVector.Length;

            double minimalCoefficient = 1.0;
            foreach (FIRARobotState obstacleRobot in _driver.firaRobots)
            {
                var obstacleRobotPoint = new Point2D(obstacleRobot.x, obstacleRobot.y);
                var ballToObstacleVector = new Vector2D(_ballPoint, obstacleRobotPoint);

                var ballToObstacleVersor = normalizeAndReturnNew(ballToObstacleVector);
                var vectorsCosinus = ballToObstacleVersor.Dot(_ballToGoalVersor);
                var complementToOneCosinus = 1 - (vectorsCosinus*vectorsCosinus);
                if (ballToObstacleVector.Length > checkAreaRadius || vectorsCosinus < 0)
                {
                    continue;
                }
                double currentCoeff;
                try
                {
                    //currentCoeff = ballToObstacleVector.Dot(_ballToGoalVector)*vectorsCosinus/normalizationCoefficient;
                    currentCoeff = _ballToGoalVector.Length*ballToObstacleVector.Length*complementToOneCosinus/normalizationCoefficient;
                }
                catch (ArithmeticException)
                {
                    continue;
                }

                minimalCoefficient = Math.Min(minimalCoefficient, currentCoeff);
            }

            //Console.WriteLine(String.Format("Val: {0}, Nor: {1}", minimalCoefficient, normalizationCoefficient));
            return CheckIntersectWithPitchSegements(new Segment2D(_ballPoint, _goalPoint))? 0:minimalCoefficient;
        }

        private double TargetInTimeReachingPart(double linearVelocity, double timeToReachLimit)
        {
            var distanceToReach = _shotLine.GetPointDistance(_robotPoint.x, _robotPoint.y);
            distanceToReach += Math.Abs(_ballPoint.x - _robotPoint.x);
            var timeToReach = distanceToReach/linearVelocity;
            var toLimitRatio = timeToReach/timeToReachLimit;
            if (toLimitRatio > 1)
            {
                return 0;
            }
            double targetInTimeReachingPart = -(toLimitRatio * toLimitRatio) + 1;
            //Console.WriteLine(String.Format("TiTRP: {0}, TLR: {1}", targetInTimeReachingPart, toLimitRatio));
            return targetInTimeReachingPart;
        }

        private double RobotBallReachabilityPart()
        {
            const double coeff = 0.2;
            var trianglePoints = new Point2D[3];
            trianglePoints[0] = _robotPoint;

            var onePartBoundaryOfCheckedAreaVector = _robotVelocityVersor;
            onePartBoundaryOfCheckedAreaVector.Rotate(Math.PI/18); //means 10 deg
            trianglePoints[1] = new Point2D(_robotPoint.x + onePartBoundaryOfCheckedAreaVector.x * coeff, _robotPoint.y + onePartBoundaryOfCheckedAreaVector.y * coeff);
            onePartBoundaryOfCheckedAreaVector.Rotate(-Math.PI/9); //means 20 deg backward
            trianglePoints[2] = new Point2D(_robotPoint.x + onePartBoundaryOfCheckedAreaVector.x * coeff, _robotPoint.y + onePartBoundaryOfCheckedAreaVector.y * coeff);

            var areaPolygon = new Polygon2D(trianglePoints);

            var toReturn = 1.0;
            foreach (FIRARobotState obstacleRobot in _driver.firaRobots)
            {
                if (obstacleRobot == _robot)
                {
                    continue;
                }
                var obstacleRobotPoint = new Point2D(obstacleRobot.x, obstacleRobot.y);
                if (areaPolygon.Contains(obstacleRobotPoint))
                {
                    toReturn = 0.0;
                }
            }

            //Console.WriteLine(String.Format("obstacles: {0}", toReturn));

            return toReturn;
        }

        private bool CheckIntersectWithPitchSegements(Segment2D line)
        {
            var intersected = areIntersected(line, Pitch.BottomSegment) ||
                               areIntersected(line, Pitch.UpperSegment) ||
                               areIntersected(line, Pitch.LeftUpperSegment) ||
                               areIntersected(line, Pitch.LeftLowerSegment) ||
                               areIntersected(line, Pitch.RightUpperSegment) ||
                               areIntersected(line, Pitch.RightLowerSegment);
            return intersected;
        }

        private bool areIntersected(Segment2D firstSeg, Segment2D secSeg)
        {
            var intPoint = firstSeg.GetIntersectionPoint(secSeg);
            return !(intPoint.x.Equals(Double.NaN) && intPoint.y.Equals(Double.NaN));
        }

        private Vector2D normalizeAndReturnNew(Vector2D vector)
        {
            var normalized = vector;
            normalized.Normalize();
            return normalized;
        }
    }
}
